package main

import (
	"fmt"
	"net/http"
	"io/ioutil"
	"encoding/json"
	"log"
)

type Slips struct {
	Slip struct {
		Advice string `json:"advice"`
		SlipID string `json:"slip_id"`
	} `json:"slip"`
}

func main()  {
	var adviceSlip Slips
	url := "https://api.adviceslip.com/advice"

	req, _ := http.NewRequest("GET", url, nil)

	res, _ := http.DefaultClient.Do(req)

	defer res.Body.Close()
	body, _ := ioutil.ReadAll(res.Body)
	err := json.Unmarshal(body, &adviceSlip)
	if err != nil {
		log.Fatal("Decoding error: ", err)
	}

	
	fmt.Println(adviceSlip.Slip.Advice)
}