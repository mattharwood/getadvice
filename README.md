# getadvice

A simple Go app to fetch a random slip of advice from adviceslip.com

## To build / install:

Install Go 1.11 in your environment

git clone gitlab.com/mattharwood/getadvice

cd getadvice

go install getadvice


## To run: 
getadvice
